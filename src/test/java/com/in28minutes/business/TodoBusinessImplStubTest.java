package com.in28minutes.business;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.in28minutes.data.api.TodoService;
import com.in28minutes.data.api.TodoServicesStub;

public class TodoBusinessImplStubTest {

	@Test
	public void testRetrieveToddodsRelatedToSpring_usingAStub() {
		TodoService todoServicestub = new TodoServicesStub();
		TodoBusinessImpl todoBusinessImpl = new TodoBusinessImpl(todoServicestub);
		List<String> filteredTodos = todoBusinessImpl
				.retrieveTodosRelatedToSpring("Dummy");
		assertEquals(2, filteredTodos.size());
	}

}
